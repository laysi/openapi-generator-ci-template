# OpenAPI Generator CI Template
OpenAPI 相關自動化CI模板

## 使用說明

1. 開新的專案
2. 複製下列檔案
   - config
   - api.yaml
   - Example.gitlab-ci.yml > .gitlab-ci.yml

3. 接著添加template模板資料 (專用模板都會放在template這個庫)
   - git submodule add ../../../opentracing-openapi-generator-template.git template  
   - git submodule update --init --recursive  

4. 根據需求修改 .gitlab-ci.yml
   - GO 下列這兩個generaotr取其一，不然會衝突
     - Go Generator
       ```yaml
       variables:
         BUILD_GO_SDK: "true"
       ```
       根據情況修改 `config/go.json`，`"packageVersion":"<packageVersion>"`不要修改，ci會自動替換`<packageVersion>`
     - Go Experimental Generator (建議使用這個)
       ```yaml
       variables:
         BUILD_GO_EXP_SDK: "true"
       ```  
       根據情況修改 `config/go-experimental.json`，`"packageVersion":"<packageVersion>"`不要修改，ci會自動替換`<packageVersion>`
   - Typescript Angular Generator
       ```yaml
       variables:
         BUILD_TYPESCRIPT_ANGULAR_SDK: "true"
       ```  
       根據情況修改 `config/typescript-angular.json`  
       npmName必須改成對應的包名   
       第一個`<replace-it>`以產品為單位scope(現階段只有amazing-vp而已)，第二個為API專案名稱  
       比如說專案名 amazing-vp/api-definition/player-history-service-api 為例
       
       ```json
       {
           "npmName":"@innova-amazing-vp/player-history-service-api-angular-sdk"
       }
       ```
5. 刪除沒用到的config檔案
6. 創建對應的git庫  
   根據你選擇的generator在[SDK](https://gitlab.enmd.net/engineering/amazing-vp/sdk)下創建對應的庫名
   - `<api repo name> Go Sdk`  
   - `<api repo name> Typescript Angular Sdk`  
   假設當前 API 庫叫做 `Demo Service API` 網址是 `https://gitlab.enmd.net/engineering/amazing-vp/api-definition/demo-service-api`  
   那 SDK 庫的名稱就叫做 `Demo Service API Go Sdk` 網址是 `https://gitlab.enmd.net/engineering/amazing-vp/api-definition/demo-service-api-go-sdk`
7. 將 cibot 添加為 SDK 庫的 Maintainer 成員
## Go Gin Server 操作建議
### windows command for go-gin-server
```cmd
docker run --rm -v %CD%:/local openapitools/openapi-generator-cli generate -i /local/api.yaml -g go-gin-server -t /local/template/go-gin-server -o /local/server
```
建議將  go-gin-server 的 model 用 go-experimental 產生器的model替換，go-experimental的model相對完善  
### windows command for go-experimental
```cmd
docker run --rm -v %CD%:/local openapitools/openapi-generator-cli generate -i /local/api.yaml -g go-experimental -t /local/template/go-experimental -c /local/config/go-experimental.json -o /local/out
```
1. 將go目錄重新命名成server
2. 將原go目錄下的所有包的package換成server
3. `go mod init <service name>`
4. go.mod 添加`// +heroku goVersion go1.13.8` 暫時迴避heroku build pack1.14無法正常運作的問題
5. 將main.go引用的 `"./go"` 改成 `"<service name>/server"`
6. 將從 go-experimental 產生的 model 檔案替換 go-gin-server 的 model 檔案，這些檔案會以 `model_` 開頭
7. 視情況將  go-experimental 產生的 utils.go 也一併複製過去，如果你結構有非required屬性的話，會產生指針結構，會需要 utils.go 的輔助函式
8. 執行 go install 初始化 go.mod 依賴